import React, { useState } from "react";
import Navbar from "./components/Navbar";
import SearchField from "./components/SearchField";
import SearchList from "./components/SearchList";
import { ISearchRequest } from "./interfaces";

const App: React.FC = () => {
  const [searchItems, setSearchItems] = useState<ISearchRequest[]>([]);
  const [searchTarget, setSearchTarget] = useState<string>("");

  const searchHandler = (title: string, searchResult: number) => {
    const newSearchItem: ISearchRequest = {
      title,
      id: Date.now(),
      searchResult
    };
    setSearchItems(prev => [newSearchItem, ...prev]);
  };

  const removeHandler = (id: number) => {
    setSearchItems(prev => prev.filter(todo => todo.id !== id));
  };

  const getTargetByClick = (target: string) => {
    setSearchTarget(target);
  };

  const clickCleanHandler = () => {
    setSearchItems([]);
  }

  return (
    <>
      <Navbar />
      <div className="container">
        <SearchField onSearch={searchHandler} searchTarget={searchTarget} />
        {searchItems.length ? (
          <div className="clean-button" onClick={clickCleanHandler}>
            <a className="waves-effect blue lighten-2 btn">
              <i className="material-icons right">clear_all</i>Clean history
            </a>
          </div>
        ) : (
          ""
        )}
        <SearchList
          searchItems={searchItems}
          onRemove={removeHandler}
          getTargetByClick={getTargetByClick}
        />
      </div>
    </>
  );
};

export default App;
