import React, { useState, useEffect } from "react";
import axios from "axios";

import ImagesGrid from "../ImagesGrid";
import config from "../../config/default";

const { flickrKey, pixabayKey } = config.apiKeys;

interface SearchFieldProps {
  onSearch(title: string, searchResult: number): void;
  searchTarget: string;
}

const SearchField: React.FC<SearchFieldProps> = ({
  onSearch,
  searchTarget
}) => {
  const [title, setTitle] = useState<string>("");
  const [images, setImages] = useState<
    {
      secret?: string;
      owner?: number;
      id?: number;
      webformatURL?: string;
      farm?: number;
      server?: number;
    }[]
  >([]);
  const [searchResults, setSearchResults] = useState<number>(0);

  const changeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(event.target.value);
  };

  const shuffleImages = (images: {}[]) => {
    let currentIndex: number = images.length,
      temporaryValue,
      randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = images[currentIndex];
      images[currentIndex] = images[randomIndex];
      images[randomIndex] = temporaryValue;
    }

    return images;
  };

  const getImages = async (target: string) => {
    try {
      // Pixabay API raw request
      const pixabayImagesRaw = await axios.get(
        `https://pixabay.com/api/?key=${pixabayKey}&q=${target}&image_type=photo&per_page=5&page=1`
      );

      // Flickr API raw request
      const flickrImagesRaw = await axios.request({
        url: `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${flickrKey}&text=${target}&sort=relevance&privacy_filter=1&safe_search=1&per_page=5&page=1&format=json&nojsoncallback=1`
      });

      const pixabayImages = pixabayImagesRaw.data.hits;
      const flickrImages = flickrImagesRaw.data.photos.photo;

      const imagesFromPixabay = pixabayImagesRaw.data.total;
      const imagesFromFlickr = parseInt(flickrImagesRaw.data.photos.total);
      const totalImagesFromResponse = imagesFromPixabay + imagesFromFlickr;

      const images = shuffleImages([...flickrImages, ...pixabayImages]);
      setImages(images);
      setSearchResults(totalImagesFromResponse.toLocaleString());
      
      onSearch(target, totalImagesFromResponse.toLocaleString());
      
    } catch (err) {
      console.log("Error during fetching the images:", err);
    }
    setTitle("");
  };

  const keyPressHandler = (event: React.KeyboardEvent) => {
    if (event.key === "Enter" && title !== "") {
      getImages(title);
    }
  };

  const clickSearchHandler = () => {
    if (title) {
      getImages(title);
    }
  };

  useEffect(() => {
    const renderTargetImages = () => {
      if (searchTarget !== '') {
        getImages(searchTarget);
      }
    }
    renderTargetImages();
  }, [searchTarget]);

  return (
    <div>
      <div className="input-field mt2">
        <input
          onChange={changeHandler}
          type="text"
          id="title"
          placeholder="Find the image ..."
          value={title}
          onKeyPress={keyPressHandler}
        />
        <label htmlFor="title" className="active">
          Find the image ...
        </label>
        <div onClick={clickSearchHandler}>
          {title ? (
            <a className="waves-effect blue lighten-2 btn">
              <i className="material-icons right">search</i>Search
            </a>
          ) : (
            <a className="waves-effect blue lighten-2 btn disabled">
              <i className="material-icons right">search</i>Search
            </a>
          )}
        </div>
        <ImagesGrid images={images}/>
      </div>
    </div>
  );
};


export default SearchField;