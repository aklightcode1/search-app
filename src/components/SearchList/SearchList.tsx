import React from "react";
import { ISearchRequest } from "../../interfaces";
import moment from "moment";

type SearchListProps = {
  searchItems: ISearchRequest[];
  onRemove: (id: number) => void;
  getTargetByClick: (target: string) => void;
};

const SearchList: React.FC<SearchListProps> = ({
  searchItems,
  onRemove,
  getTargetByClick
}) => {
  if (searchItems.length === 0) {
    return <p className="center">There are no any saved searches yet</p>;
  }

  const clickHandler = (item: ISearchRequest) => {
    getTargetByClick(item.title);
  };

  return (
    <div className="search-table" data-testid="search-list">
      <table className="centered highlight">
        <thead>
          <tr>
            <th>Target</th>
            <th>Time</th>
            <th>Results</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {searchItems.map(item => {
            return (
              <tr
                className="search-item"
                key={item.id++}
                onClick={() => clickHandler(item)}
              >
                <td>{item.title}</td>
                <td>{moment(item.id).format("LLL")}</td>
                <td>{item.searchResult}</td>
                <td>
                  {/* <div>
                    <i
                      className="material-icons red-text"
                      onClick={() => onRemove(item.id)}
                    >
                      delete
                    </i>
                  </div> */}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default SearchList;
