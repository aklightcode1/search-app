import React from "react";
import ReactDOM from "react-dom";
import SearchList from "./../SearchList";

import { render, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import renderer from "react-test-renderer";

afterEach(cleanup);

describe("<SearchList/>", () => {
    it("renders without crashing", () => {
        const div = document.createElement("div");

        ReactDOM.render(<SearchList searchItems={[{}]} />, div);
    });

    it("renders correctly", () => {
        const { getByTestId } = render(<SearchList searchItems={[{}]} />);
        expect(getByTestId("search-list")).toHaveClass('search-table');
    });

    it("matches snapshot", () => {
        const tree = renderer.create(<SearchList searchItems={[{}]} />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
