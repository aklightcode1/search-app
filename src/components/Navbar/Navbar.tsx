import React from "react";

 const Navbar: React.FC = () => (
  <nav>
    <div className="nav-wrapper blue lighten-1 px1 center" data-testid="navbar">
      <a href="/" className="brand-logo"></a>
    </div>
  </nav>
);

export default Navbar;