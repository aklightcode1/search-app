import React from "react";
import InfiniteScroll from 'react-infinite-scroller';

interface ImagesGridProps {
  images: {
    secret?: string;
    owner?: number;
    id?: number;
    webformatURL?: string;
    farm?: number;
    server?: number;
  }[];
}
 const ImagesGrid: React.FC<ImagesGridProps> = ({images}) => {
  return (
    <div className="image-grid-wrapper">
      {images.map(image => {
        return (
          <div className="image" key={image.id}>
            <img
              
              src={
                image.secret
                  ? `https://farm${image.farm}.staticflickr.com/${image.server}/${image.id}_${image.secret}_z.jpg`
                  : image.webformatURL
              }
              alt=""
            />
          </div>
        );
      })}
    </div>
  );
};


export default ImagesGrid;