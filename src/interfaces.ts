export interface ISearchRequest {
    title: string
    id: number
    searchResult: number
}